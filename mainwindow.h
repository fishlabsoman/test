#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void valueChanged();

    void on_pushButton_clicked();

    void readData();

private:
    Ui::MainWindow *ui;
    void set_color();
    void fillPortsInfo();
    //void handleError(QSerialPort::SerialPortError error);
};

#endif // MAINWINDOW_H
