#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qdebug.h>

#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>

#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

QSerialPort* serial = new QSerialPort();
QString str = "";
bool connected = false;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);

    connect(ui->slider_red,         SIGNAL(valueChanged(int)), this, SLOT(valueChanged()));
    connect(ui->slider_green,       SIGNAL(valueChanged(int)), this, SLOT(valueChanged()));
    connect(ui->slider_blue,        SIGNAL(valueChanged(int)), this, SLOT(valueChanged()));
    connect(ui->slider_brightness,  SIGNAL(valueChanged(int)), this, SLOT(valueChanged()));

    connect(serial, SIGNAL(readyRead()), this, SLOT(readData()));

    set_color();
    fillPortsInfo();
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::valueChanged() {
    QSlider *obj = (QSlider*)(QObject::sender());
    qDebug() << QString("%0\t%1").arg(obj->objectName()).arg(obj->value());

    set_color();
}


void MainWindow::set_color() {
    ui->widget_test->setStyleSheet(QString("background-color:rgb(%0,%1,%2)")
                                   .arg(ui->slider_red->value())
                                   .arg(ui->slider_green->value())
                                   .arg(ui->slider_blue->value()));

    QString str1 = QString("rgb %0%1%2\r\n")
                   .arg(ui->slider_red->value(), 2, 16, QChar('0'))
                   .arg(ui->slider_green->value(), 2, 16, QChar('0'))
                   .arg(ui->slider_blue->value(), 2, 16, QChar('0'));


    char * str;
    QByteArray ba;
    ba = str1.toLatin1();
    str = ba.data();
    qDebug() << str;
    serial->write(str);



    str1 = QString("intensity %0\r\n")
           .arg(ui->slider_brightness->value());



    ba = str1.toLatin1();
    str = ba.data();
    qDebug() << str;
    serial->write(str);

}





void MainWindow::fillPortsInfo() {
    ui->serialPortInfoListBox->clear();
    static const QString blankString = QObject::tr("N/A");
    QString description;
    QString manufacturer;
    QString serialNumber;
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        QStringList list;
        description = info.description();
        manufacturer = info.manufacturer();
        serialNumber = info.serialNumber();
        list << info.portName()
             << (!description.isEmpty() ? description : blankString)
             << (!manufacturer.isEmpty() ? manufacturer : blankString)
             << (!serialNumber.isEmpty() ? serialNumber : blankString)
             << info.systemLocation()
             << (info.vendorIdentifier() ? QString::number(info.vendorIdentifier(), 16) : blankString)
             << (info.productIdentifier() ? QString::number(info.productIdentifier(), 16) : blankString);

        ui->serialPortInfoListBox->addItem(list.first(), list);
    }
}




void MainWindow::on_pushButton_clicked() {



    serial->close();
    serial->disconnect();

    serial->setPortName(ui->serialPortInfoListBox->currentText());
    serial->setBaudRate(QSerialPort::Baud115200);
    /*serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);
    */
    if (serial->open(QIODevice::ReadWrite)) {
        set_color();
        ui->label_connected->setText("Connected");
        connected = true;
        qDebug() << "okay";

    } else {

        ui->label_connected->setText("Disconnected");
        connected = false;
        qDebug() << "error";
    }

    /*

    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        qDebug() << "Name        : " << info.portName();
        qDebug() << "Description : " << info.description();
        qDebug() << "Manufacturer: " << info.manufacturer();

        // Example use QSerialPort
        QSerialPort serial;
        serial.setPort(info);
        serial.setBaudRate(QSerialPort::Baud9600);

        serial.setDataBits(QSerialPort::Data8);
        serial.setParity(QSerialPort::NoParity);
        serial.setStopBits(QSerialPort::OneStop);
        serial.setFlowControl(QSerialPort::NoFlowControl);


        serial.write("1");
        if (serial.open(QIODevice::ReadWrite)) {

                //serial.write("test\n",6);


            serial.close();
        } else {
            qDebug() << "not connected";
        }
    }
    */


    fillPortsInfo();

}

/*
void MainWindow::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) {
        QMessageBox::critical(this, tr("Critical Error"), serial->errorString());
        closeSerialPort();
    }
}
*/

void MainWindow::readData() {

    str += serial->readLine();

    if(str.contains("\n")) {
        str = str.trimmed();
        qDebug() << str;
        str = "";
    }

}
