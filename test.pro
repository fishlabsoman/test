#-------------------------------------------------
#
# Project created by QtCreator 2015-02-02T18:52:41
#
#-------------------------------------------------

QTPLUGIN += qsqloci qgif
CONFIG += static

include('./qtserialport/serialport-src/src/serialport/qt4support/serialport.prf')

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
